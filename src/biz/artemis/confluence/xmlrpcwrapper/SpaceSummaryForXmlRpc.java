package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Hashtable;

/**
 * 'Xml Rpc Confluence Space Summary Hashtable Wrapper' is the real description.
 *
 * Encapsulates data and functionality around a SpaceForXmlRpc which gets
 * moved to and from Confluence via XMLRPC
 * <p/>
 * Using this class which just wraps the Hashtable that Confluence returns for
 * a page has several advantages including reducing errors from possible
 * String typos...also lets you use code complete in your favorite IDEA
 * and a Hashtable is a good thing to abstract.
 *
 * I'm not sure I like the name of this class. It probably should have been
 * something like SpaceWrapperForXmlRpc.  I definitely wanted it start with the
 * word 'Page', but 'Xml Rpc Space Wrapper' is the real description.
 */
public class SpaceSummaryForXmlRpc {

    /**
     * the id of the space
     */
    protected final String ID = "id";
    /**
     * the key of the space
     */
    protected final String SPACE_KEY = "key";
    /**
     * the name of the space
     */
    protected final String SPACE_NAME = "name";
    /**
     * the url to view this space online
     */
    protected final String URL = "url";

    /**
     * the url to view this space online
     */
    protected final String TYPE = "type";
    /**
     * the homepage of this space
     */
    protected final String HOMEPAGE = "homepage";
//    /**
//     * the space description content
//     */
//    protected final String DESCRIPTION = "description";

    /**
     * creates a PageForXmlRpc. Since the values already exist in the
     * hasttable we just wrap that and reference it rather than
     * copying all the values. This reduces possible typos.
     * @param pageHT
     * @return a pageforxmlrpc obj
     */
    public static SpaceSummaryForXmlRpc create(Hashtable<String, String> spaceSummaryHT) {
        SpaceSummaryForXmlRpc space = new SpaceSummaryForXmlRpc();
        space.setSpaceParams(spaceSummaryHT);
        return space;
    }
    /**
     * This is the Hashtable holding the instance variables associated with
     * a page. Confluence-XMLRPC expects a Hashtable.
     */
    Hashtable<String, String> spaceParams = new Hashtable<String, String>();

    public Hashtable<String, String> getSpaceParams() {
        return spaceParams;
    }

    public void setSpaceParams(Hashtable spaceParams) {
        this.spaceParams = spaceParams;
    }

    public String getId() {
        return String.valueOf(spaceParams.get(ID));
    }

    public void setId(String idVal) {
        spaceParams.put(ID, idVal);
    }

    public String getSpaceName() {
        return String.valueOf(spaceParams.get(SPACE_NAME));
    }

    public void setSpaceName(String spaceNameVal) {
        spaceParams.put(SPACE_NAME, spaceNameVal);
    }

    public String getSpaceKey() {
        return String.valueOf(spaceParams.get(SPACE_KEY));
    }

    public void setSpaceKey(String spaceKey) {
        spaceParams.put(SPACE_KEY, spaceKey);
    }

    public String getURL() {
        return String.valueOf(spaceParams.get(URL));
    }

    public void setURL(String urlVal) {
        spaceParams.put(URL, urlVal);
    }

    public String getType() {
        return String.valueOf(spaceParams.get(TYPE));
    }

    public void setType(String type) {
        spaceParams.put(TYPE, type);
    }

    public String getHomepage() {
        return String.valueOf(spaceParams.get(HOMEPAGE));
    }

    public void setHomepage(String homepageVal) {
        spaceParams.put(HOMEPAGE, homepageVal);
    }

    public String toString() {
        return getSpaceName();
    }

//    public String getDescription() {
//        return String.valueOf(spaceParams.get(DESCRIPTION));
//    }
//
//    public void setDescription(String descriptionVal) {
//        spaceParams.put(DESCRIPTION, descriptionVal);
//    }

}