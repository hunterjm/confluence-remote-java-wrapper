package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Hashtable;

/**
 * See PageForXmlRpc for a detailed description of this class's purpose.
 * This is another wrapper
 * class for what the Confluence Remote API hands back as a PageHistorySummary
 *
 */
public class PageHistorySummaryForXmlRpc {

    /**
     * the id of the page
     */
    protected final String ID = "id";
    /**
     * the key of the space that this page belongs to
     */
    /**
     * the version number of this page
     */
    protected final String VERSION = "version";
    /**
     * timestamp page was modified
     */
    protected final String MODIFIED = "modified";
    /**
     * username of the page's last modifier
     */
    protected final String MODIFIER = "modifier";

    /**
     * creates a PageForXmlRpc. Since the values already exist in the
     * hasttable we just wrap that and reference it rather than
     * copying all the values. This reduces possible typos.
     * @param pageHT
     * @return a pageforxmlrpc obj
     */
    public static PageHistorySummaryForXmlRpc create(Hashtable pageHT) {
        PageHistorySummaryForXmlRpc page = new PageHistorySummaryForXmlRpc();
        page.setPageParams(pageHT);
        return page;
    }
    /**
     * This is the Hashtable holding the instance variables associated with
     * a page. Confluence-XMLRPC expects a Hashtable.
     */
    Hashtable<String, String> pageParams = new Hashtable<String, String>();

    public Hashtable<String, String> getPageParams() {
        return pageParams;
    }

    public void setPageParams(Object pageParams) {
        this.pageParams = (Hashtable<String, String>) pageParams;
    }

    public String getId() {
        return String.valueOf(pageParams.get(ID));
    }

    public void setId(String idVal) {
        pageParams.put(ID, idVal);
    }


    public String getVersion() {
        return String.valueOf(pageParams.get(VERSION));
    }

    public void setVersion(String versionVal) {
        pageParams.put(VERSION, versionVal);
    }


    public String getModified() {
        return String.valueOf(pageParams.get(MODIFIED));
    }

    public void setModified(String modifiedVal) {
        pageParams.put(MODIFIED, modifiedVal);
    }

    public String getModifier() {
        return String.valueOf(pageParams.get(MODIFIER));
    }

    public void setModifier(String modifierVal) {
        pageParams.put(MODIFIER, modifierVal);
    }

}