package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Hashtable;

/**
 * 'Xml Rpc Confluence Page Hashtable Wrapper' is the real description.
 *
 * Encapsulates data and functionality around a PageForXmlRpc which gets
 * moved to and from Confluence via XMLRPC
 * <p/>
 * Using this class which just wraps the Hashtable that Confluence returns for
 * a page has several advantages including reducing errors from possible
 * String typos...also lets you use code complete in your favorite IDEA
 * and a Hashtable is a good thing to abstract.
 *
 * I'm not sure I like the name of this class. It probably should have been
 * something like PageWrapperForXmlRpc.  I definitely wanted it start with the
 * word 'Page', but 'Xml Rpc Page Wrapper' is the real description.
 */
public class PageForXmlRpc {

    /**
     * the id of the page
     */
    protected final String ID = "id";
    /**
     * the key of the space that this page belongs to
     */
    protected final String SPACE = "space";
    /**
     * the id of the parent page
     */
    protected final String PARENT_ID = "parentId";
    /**
     * the title of the page
     */
    protected final String TITLE = "title";
    /**
     * the url to view this page online
     */
    protected final String URL = "url";
    /**
     * the version number of this page
     */
    protected final String VERSION = "version";
    /**
     * the page content
     */
    protected final String CONTENT = "content";
    /**
     * timestamp page was created
     */
    protected final String CREATED = "created";
    /**
     * username of the creator
     */
    protected final String CREATOR = "creator";
    /**
     * timestamp page was modified
     */
    protected final String MODIFIED = "modified";
    /**
     * username of the page's last modifier
     */
    protected final String MODIFIER = "modifier";
    /**
     * whether or not this page is the space's homepage
     */
    protected final String HOMEPAGE = "homePage";
    /**
     * the number of locks current on this page
     */
    protected final String LOCKS = "locks";
    /**
     * status of the page (eg. current or deleted)
     */
    protected final String CONTENT_STATUS = "contentStatus";
    /**
     * whether the page is current and not deleted
     */
    protected final String CURRENT = "current";
    private String parentTitle;

    /**
     * creates a PageForXmlRpc. Since the values already exist in the
     * hasttable we just wrap that and reference it rather than
     * copying all the values. This reduces possible typos.
     * @param pageHT
     * @return a pageforxmlrpc obj
     */
    public static PageForXmlRpc create(Hashtable pageHT) {
        PageForXmlRpc page = new PageForXmlRpc();
        page.setPageParams(pageHT);
        return page;
    }
    /**
     * This is the Hashtable holding the instance variables associated with
     * a page. Confluence-XMLRPC expects a Hashtable.
     */
    Hashtable<String, String> pageParams = new Hashtable<String, String>();

    public Hashtable<String, String> getPageParams() {
        return pageParams;
    }

    public void setPageParams(Object pageParams) {
        this.pageParams = (Hashtable<String, String>) pageParams;
    }

    public String getId() {
        return String.valueOf(pageParams.get(ID));
    }

    public void setId(String idVal) {
        pageParams.put(ID, idVal);
    }


    public void removeId() {
        pageParams.remove(ID);
    }

    public String getSpace() {
        return String.valueOf(pageParams.get(SPACE));
    }

    public void setSpace(String spaceVal) {
        pageParams.put(SPACE, spaceVal);
    }

    public String getParentId() {
        return String.valueOf(pageParams.get(PARENT_ID));
    }

    public void setParentId(String parentIdVal) {
        pageParams.put(PARENT_ID, parentIdVal);
    }

    public String getTitle() {
        return String.valueOf(pageParams.get(TITLE));
    }

    public void setTitle(String titleVal) {
        pageParams.put(TITLE, titleVal);
    }

    public String getUrl() {
        return String.valueOf(pageParams.get(URL));
    }

    public void setUrl(String urlVal) {
        pageParams.put(URL, urlVal);
    }

    public String getVersion() {
        return String.valueOf(pageParams.get(VERSION));
    }

    public void setVersion(String versionVal) {
        pageParams.put(VERSION, versionVal);
    }

    public String getContent() {
        return String.valueOf(pageParams.get(CONTENT));
    }

    public void setContent(String contentVal) {
        pageParams.put(CONTENT, contentVal);
    }

    public String getCreated() {
        return String.valueOf(pageParams.get(CREATED));
    }

    public void setCreated(String createdVal) {
        pageParams.put(CREATED, createdVal);
    }

    public String getCreator() {
        return String.valueOf(pageParams.get(CREATOR));
    }

    public void setCreator(String creatorVal) {
        pageParams.put(CREATOR, creatorVal);
    }

    public String getModified() {
        return String.valueOf(pageParams.get(MODIFIED));
    }

    public void setModified(String modifiedVal) {
        pageParams.put(MODIFIED, modifiedVal);
    }

    public String getModifier() {
        return String.valueOf(pageParams.get(MODIFIER));
    }

    public void setModifier(String modifierVal) {
        pageParams.put(MODIFIER, modifierVal);
    }

    public String getHomePage() {
        return String.valueOf(pageParams.get(HOMEPAGE));
    }

    public void setHomePage(String homePageVal) {
        pageParams.put(HOMEPAGE, homePageVal);
    }

    public String getLocks() {
        return String.valueOf(pageParams.get(LOCKS));
    }

    public void setLocks(String locksVal) {
        pageParams.put(LOCKS, locksVal);
    }

    public String getContentStatus() {
        return String.valueOf(pageParams.get(CONTENT_STATUS));
    }

    public void setContentStatus(String contentStatusVal) {
        pageParams.put(CONTENT_STATUS, contentStatusVal);
    }

    public String getCurrent() {
        return String.valueOf(pageParams.get(CURRENT));
    }

    public void setCurrent(String currentVal) {
        pageParams.put(CURRENT, currentVal);
    }

    public String toString() {
        return this.getTitle();
    }

    public PageForXmlRpc cloneForNewPage(String spaceKey) {
        PageForXmlRpc newPage = new PageForXmlRpc();
        newPage.pageParams.put(SPACE, spaceKey);
        newPage.pageParams.put(TITLE, getTitle());
        newPage.pageParams.put(CONTENT, getContent());
        return newPage;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentTitle() {
        return parentTitle;
    }
}
