package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Hashtable;

/**
 * 'Xml Rpc Confluence blog Hashtable Wrapper' is the real description.
 *
 * Encapsulates data and functionality around a blogForXmlRpc which gets
 * moved to and from Confluence via XMLRPC
 * <p/>
 * Using this class which just wraps the Hashtable that Confluence returns for
 * a blog has several advantages including reducing errors from possible
 * String typos...also lets you use code complete in your favorite IDEA
 * and a Hashtable is a good thing to abstract.
 *
 * I'm not sure I like the name of this class. It probably should have been
 * something like blogWrapperForXmlRpc.  I definitely wanted it start with the
 * word 'blog', but 'Xml Rpc blog Wrapper' is the real description.
 *
 * // * id 	String 	the id of the blog entry
 * //space 	String 	the key of the space that this blog entry belongs to
 * //title 	String 	the title of the page
 * //url 	String 	the url to view this blog entry online
 * //version 	int 	the version number of this blog entry
 * //content 	String 	the blog entry content
 * //locks 	int 	the number of locks current on this page
 */
public class BlogForXmlRpc {

    /**
     * the id of the blog
     */
    protected final String ID = "id";
    /**
     * the key of the space that this blog belongs to
     */
    protected final String SPACE = "space";
//    /**
//     * the id of the parent blog
//     */
//    protected final String PARENT_ID = "parentId";
    /**
     * the title of the blog
     */
    protected final String TITLE = "title";
    /**
     * the url to view this blog online
     */
    protected final String URL = "url";
    /**
     * the version number of this blog
     */
    protected final String VERSION = "version";
    /**
     * the blog content
     */
    protected final String CONTENT = "content";
    /**
     * timestamp blog was created
     */
//    protected final String CREATED = "created";
//    /**
//     * username of the creator
//     */
//    protected final String CREATOR = "creator";
//    /**
//     * timestamp blog was modified
//     */
//    protected final String MODIFIED = "modified";
//    /**
//     * username of the blog's last modifier
//     */
    protected final String MODIFIER = "modifier";
    /**
     * whether or not this blog is the space's homeblog
     */
    protected final String HOMEblog = "homeblog";
    /**
     * the number of locks current on this blog
     */
    protected final String LOCKS = "locks";
    /**
     * status of the blog (eg. current or deleted)
     */
    protected final String CONTENT_STATUS = "contentStatus";
    /**
     * whether the blog is current and not deleted
     */
    protected final String CURRENT = "current";
    private String parentTitle;

    /**
     * creates a blogForXmlRpc. Since the values already exist in the
     * hasttable we just wrap that and reference it rather than
     * copying all the values. This reduces possible typos.
     * @param blogHT
     * @return a blogforxmlrpc obj
     */
    public static BlogForXmlRpc create(Hashtable blogHT) {
        BlogForXmlRpc blog = new BlogForXmlRpc();
        blog.setblogParams(blogHT);
        return blog;
    }
    /**
     * This is the Hashtable holding the instance variables associated with
     * a blog. Confluence-XMLRPC expects a Hashtable.
     */
    Hashtable<String, String> blogParams = new Hashtable<String, String>();

    public Hashtable<String, String> getblogParams() {
        return blogParams;
    }

    public void setblogParams(Object blogParams) {
        this.blogParams = (Hashtable<String, String>) blogParams;
    }

    public String getId() {
        return String.valueOf(blogParams.get(ID));
    }

    public void setId(String idVal) {
        blogParams.put(ID, idVal);
    }


    public void removeId() {
        blogParams.remove(ID);
    }

    public String getSpace() {
        return String.valueOf(blogParams.get(SPACE));
    }

    public void setSpace(String spaceVal) {
        blogParams.put(SPACE, spaceVal);
    }

//    public String getParentId() {
//        return String.valueOf(blogParams.get(PARENT_ID));
//    }
//
//    public void setParentId(String parentIdVal) {
//        blogParams.put(PARENT_ID, parentIdVal);
//    }

    public String getTitle() {
        return String.valueOf(blogParams.get(TITLE));
    }

    public void setTitle(String titleVal) {
        blogParams.put(TITLE, titleVal);
    }

    public String getUrl() {
        return String.valueOf(blogParams.get(URL));
    }

    public void setUrl(String urlVal) {
        blogParams.put(URL, urlVal);
    }

    public String getVersion() {
        return String.valueOf(blogParams.get(VERSION));
    }

    public void setVersion(String versionVal) {
        blogParams.put(VERSION, versionVal);
    }

    public String getContent() {
        return String.valueOf(blogParams.get(CONTENT));
    }

    public void setContent(String contentVal) {
        blogParams.put(CONTENT, contentVal);
    }

//    public String getCreated() {
//        return String.valueOf(blogParams.get(CREATED));
//    }
//
//    public void setCreated(String createdVal) {
//        blogParams.put(CREATED, createdVal);
//    }
//
//    public String getCreator() {
//        return String.valueOf(blogParams.get(CREATOR));
//    }
//
//    public void setCreator(String creatorVal) {
//        blogParams.put(CREATOR, creatorVal);
//    }
//
//    public String getModified() {
//        return String.valueOf(blogParams.get(MODIFIED));
//    }
//
//    public void setModified(String modifiedVal) {
//        blogParams.put(MODIFIED, modifiedVal);
//    }

    public String getModifier() {
        return String.valueOf(blogParams.get(MODIFIER));
    }

    public void setModifier(String modifierVal) {
        blogParams.put(MODIFIER, modifierVal);
    }

    public String getHomeblog() {
        return String.valueOf(blogParams.get(HOMEblog));
    }

    public void setHomeblog(String homeblogVal) {
        blogParams.put(HOMEblog, homeblogVal);
    }

    public String getLocks() {
        return String.valueOf(blogParams.get(LOCKS));
    }

    public void setLocks(String locksVal) {
        blogParams.put(LOCKS, locksVal);
    }

    public String getContentStatus() {
        return String.valueOf(blogParams.get(CONTENT_STATUS));
    }

    public void setContentStatus(String contentStatusVal) {
        blogParams.put(CONTENT_STATUS, contentStatusVal);
    }

    public String getCurrent() {
        return String.valueOf(blogParams.get(CURRENT));
    }

    public void setCurrent(String currentVal) {
        blogParams.put(CURRENT, currentVal);
    }

    public String toString() {
        return this.getTitle();
    }

    public BlogForXmlRpc cloneForNewblog(String spaceKey) {
        BlogForXmlRpc newblog = new BlogForXmlRpc();
        newblog.blogParams.put(SPACE, spaceKey);
        newblog.blogParams.put(TITLE, getTitle());
        newblog.blogParams.put(CONTENT, getContent());
        return newblog;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentTitle() {
        return parentTitle;
    }
}
