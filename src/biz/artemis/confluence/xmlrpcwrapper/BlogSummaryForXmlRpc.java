package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Hashtable;

/**
 * See PageForXmlRpc for a detailed description. This is another wrapper
 * class for what the Confluence Remote API hands back as a PageSummary
 * http://confluence.atlassian.com/display/CONFDEV/Remote+API+Specification#RemoteAPISpecification-BlogEntrySummary
 *
 */
public class BlogSummaryForXmlRpc {


//    	Type 	Value
//id 	String 	the id of the blog entry
//space 	String 	the key of the space that this blog entry belongs to
//title 	String 	the title of the blog entry
//url 	String 	the url to view this blog entry online
//locks 	int 	the number of locks current on this page
//publishDate 	Date 	the date the blog post was published

    /**
     * the id of the page
     */
    protected final String ID = "id";
    /**
     * the key of the space that this page belongs to
     */
    protected final String SPACE = "space";
//    /**
//     * the id of the parent page
//     */
//    protected final String PARENT_ID = "parentId";
    /**
     * the title of the page
     */
    protected final String TITLE = "title";
    /**
     * the url to view this page online
     */
    protected final String URL = "url";
    /**
     * the number of locks current on this page
     */
    protected final String LOCKS = "locks";

    /**
     * creates a PageForXmlRpc. Since the values already exist in the
     * hasttable we just wrap that and reference it rather than
     * copying all the values. This reduces possible typos.
     * @param pageHT
     * @return a pageforxmlrpc obj
     */
    public static BlogSummaryForXmlRpc create(Hashtable pageHT) {
        BlogSummaryForXmlRpc page = new BlogSummaryForXmlRpc();
        page.setPageParams(pageHT);
        return page;
    }
    /**
     * This is the Hashtable holding the instance variables associated with
     * a page. Confluence-XMLRPC expects a Hashtable.
     */
    Hashtable<String, String> pageParams = new Hashtable<String, String>();

    public Hashtable<String, String> getPageParams() {
        return pageParams;
    }

    public void setPageParams(Object pageParams) {
        this.pageParams = (Hashtable<String, String>) pageParams;
    }

    public String getId() {
        return String.valueOf(pageParams.get(ID));
    }

    public void setId(String idVal) {
        pageParams.put(ID, idVal);
    }

    public String getSpace() {
        return String.valueOf(pageParams.get(SPACE));
    }

    public void setSpace(String spaceVal) {
        pageParams.put(SPACE, spaceVal);
    }

    public String getTitle() {
        return String.valueOf(pageParams.get(TITLE));
    }

    public void setTitle(String titleVal) {
        pageParams.put(TITLE, titleVal);
    }

    public String getUrl() {
        return String.valueOf(pageParams.get(URL));
    }

    public void setUrl(String urlVal) {
        pageParams.put(URL, urlVal);
    }


    public String getLocks() {
        return String.valueOf(pageParams.get(LOCKS));
    }

    public void setLocks(String locksVal) {
        pageParams.put(LOCKS, locksVal);
    }


    public String toString() {
        return this.getTitle();
    }

    public BlogSummaryForXmlRpc cloneForNewPage(String spaceKey) {
        BlogSummaryForXmlRpc newPage = new BlogSummaryForXmlRpc();
        newPage.pageParams.put(SPACE, spaceKey);
        newPage.pageParams.put(TITLE, getTitle());
        return newPage;
    }

}