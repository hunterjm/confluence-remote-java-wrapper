package biz.artemis.confluence.xmlrpcwrapper.test;

import junit.framework.Test;
import junit.framework.TestSuite;
import biz.artemis.confluence.xmlrpcwrapper.RemoteWikiBrokerProtectedTest;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(
				"Tests for RemoteWikiBroker");
		//$JUnit-BEGIN$
		suite.addTestSuite(RemoteWikiBrokerTest.class); //public api tests
		suite.addTestSuite(RemoteWikiBrokerProtectedTest.class); //protected method tests
		//$JUnit-END$
		return suite;
	}

}
