package biz.artemis.confluence.xmlrpcwrapper;


/**
 * This class is just meant to encapsulate all the necessary
 * server settings. At some point in the future we might
 * want to connect to more than two servers simulatneously or
 * at least optionally and want to make this easy.
 */
public class ConfluenceServerSettings {
	/**
	 * confluence login
	 */
	public String login;
	/**
	 * confluence password
	 */
	public String password;
	/**
	 * confluence spacekey that we're connecting to
	 */
	public String spaceKey;
	/**
	 * base url to the confluence in question.
	 */
	public String url;
	/**
	 * location of truststore file corresponding with CA
	 * if confluence is protected by SSL.
	 * Leave null if not using.
	 */
	public String truststore; 
	/**
	 * password that was used when generating truststore file
	 * corresponding with CA if confluence is protected by SSL.
	 * Leave null if not using.
	 */
	public String trustpass;
	/**
	 * option to allow CRJW to trust all certs. 
	 * should be "true", "false". If null or empty, the default
	 * value is false.
	 * WARNING: If trustallcerts is set to true, use of the remote
	 * api could be vulnerable to "man in the middle attacks".
	 */
	public String trustallcerts;
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSpaceKey() {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTrustpass() {
		return trustpass;
	}

	public void setTrustpass(String trustpass) {
		this.trustpass = trustpass;
	}

	public String getTruststore() {
		return truststore;
	}

	public void setTruststore(String truststore) {
		this.truststore = truststore;
	}

	
	public String getTrustallcerts() {
		return trustallcerts;
	}

	public void setTrustallcerts(String trustallcerts) {
		this.trustallcerts = trustallcerts;
	}

}
